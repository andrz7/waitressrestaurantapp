package thesis.traininlink.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import thesis.traininlink.model.User;

import static thesis.traininlink.database.DatabaseHelper.TABLE_NAME;

/**
 * Created by Andrea's on 9/28/2016.
 */

public class UserDao {

    public void addNewTransaction(Context context, int id, String fullName, String gender, String email,
                                  String city, String phoneNumber, String address, String avatar,
                                  String facebookId, String twitterId, String latitude, String longitude) {
        SQLiteDatabase db = DatabaseHelper.getsInstance(context).getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.ID_COLUMN, id);
        cv.put(DatabaseHelper.FULLNAME_COLUMN, fullName);
        cv.put(DatabaseHelper.GENDER_COLUMN, gender);
        cv.put(DatabaseHelper.EMAIL_COLUMN, email);
        cv.put(DatabaseHelper.CITY_COLUMN, city);
        cv.put(DatabaseHelper.PHONE_COLUMN, phoneNumber);
        cv.put(DatabaseHelper.ADDRESS_COLUMN, address);
        cv.put(DatabaseHelper.AVATAR_COLUMN, avatar);
        cv.put(DatabaseHelper.FACEBOOK_ID_COLUMN, facebookId);
        cv.put(DatabaseHelper.TWITTER_ID_COLUMN, twitterId);
        cv.put(DatabaseHelper.LATITUDE_COLUMN, latitude);
        cv.put(DatabaseHelper.LONGITUDE_COLUMN, longitude);
        db.insertWithOnConflict(TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public User getData(Context context){
        User objUser = null;
        SQLiteDatabase db = DatabaseHelper.getsInstance(context).getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null,
                null, null, null);
        while (cursor.moveToNext()){
            int userId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.ID_COLUMN));
            String fullName = cursor.getString(cursor.getColumnIndex(DatabaseHelper.FULLNAME_COLUMN));
            String gender = cursor.getString(cursor.getColumnIndex(DatabaseHelper.GENDER_COLUMN));
            String email = cursor.getString(cursor.getColumnIndex(DatabaseHelper.EMAIL_COLUMN));
            String city = cursor.getString(cursor.getColumnIndex(DatabaseHelper.CITY_COLUMN));
            String phone = cursor.getString(cursor.getColumnIndex(DatabaseHelper.PHONE_COLUMN));
            String address = cursor.getString(cursor.getColumnIndex(DatabaseHelper.ADDRESS_COLUMN));
            String avatar = cursor.getString(cursor.getColumnIndex(DatabaseHelper.AVATAR_COLUMN));
            String facebook = cursor.getString(cursor.getColumnIndex(DatabaseHelper.FACEBOOK_ID_COLUMN));
            String twitter = cursor.getString(cursor.getColumnIndex(DatabaseHelper.TWITTER_ID_COLUMN));
            String latitude = cursor.getString(cursor.getColumnIndex(DatabaseHelper.LATITUDE_COLUMN));
            String longitude = cursor.getString(cursor.getColumnIndex(DatabaseHelper.LONGITUDE_COLUMN));

            objUser = new User(userId, fullName, gender, email, city, phone, address, avatar, facebook, twitter, Double.parseDouble(latitude), Double.parseDouble(longitude));
        }
        db.close();
        return objUser;
    }

    public void deleteAllData(Context context){
        SQLiteDatabase db = DatabaseHelper.getsInstance(context).getWritableDatabase();
        db.delete(DatabaseHelper.TABLE_NAME, null, null);
        db.close();
    }
}

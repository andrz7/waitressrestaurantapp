package thesis.traininlink.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Andrea's on 9/28/2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper sInstance;

    private final static String DB_NAME = "database";
    static final int VERSION = 1;

    public final static String TABLE_NAME = "MsUser";
    public static final String ID_COLUMN = "id";
    public static final String FULLNAME_COLUMN = "fullname";
    public static final String GENDER_COLUMN = "gender";
    public static final String CITY_COLUMN= "city";
    public static final String EMAIL_COLUMN = "email";
    public static final String PHONE_COLUMN = "phone";
    public static final String ADDRESS_COLUMN = "address";
    public static final String AVATAR_COLUMN = "avatar";
    public static final String FACEBOOK_ID_COLUMN = "facebook";
    public static final String TWITTER_ID_COLUMN = "twitter";
    public static final String LATITUDE_COLUMN = "latitude";
    public static final String LONGITUDE_COLUMN = "longitude";

    public static String TABLE_NAME_ORDER = "TransactionOrder";
    public static String NORMAL_ID = "id";
    public static String USER_ID = "user_id";
    public static String ORDER_ID  = "order_id";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    public static synchronized DatabaseHelper getsInstance(Context context){
        if (sInstance == null)
            sInstance = new DatabaseHelper(context.getApplicationContext());
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String create = "CREATE TABLE IF NOT EXISTS '"+TABLE_NAME+"'(\n"+
                "'"+ID_COLUMN+"' INTEGER PRIMARY KEY NOT NULL, "+"'"+FULLNAME_COLUMN+"' TEXT, "+
                "'"+GENDER_COLUMN+"' TEXT, "+"'"+EMAIL_COLUMN+"' TEXT, "+"'"+PHONE_COLUMN+"' TEXT, "+
                "'"+CITY_COLUMN+"' TEXT, "+"'"+ADDRESS_COLUMN+"' TEXT, "+"'"+AVATAR_COLUMN+"' TEXT, "+
                "'"+FACEBOOK_ID_COLUMN+"' TEXT, "+"'"+TWITTER_ID_COLUMN+"' TEXT, "+"'"+LATITUDE_COLUMN+"' TEXT, "+
                "'"+LONGITUDE_COLUMN+"' TEXT);";

        String createOrderTable = "CREATE TABLE IF NOT EXISTS '"+TABLE_NAME_ORDER+"'(\n"+
                "'"+NORMAL_ID+"' INTEGER PRIMARY KEY NOT NULL, "+"'"+USER_ID+"' INTEGER, "+"'"+ORDER_ID+"' INTEGER); ";

        sqLiteDatabase.execSQL(create);
        sqLiteDatabase.execSQL(createOrderTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

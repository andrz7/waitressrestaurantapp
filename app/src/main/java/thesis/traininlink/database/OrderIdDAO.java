package thesis.traininlink.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Andrea's on 10/1/2016.
 */

public class OrderIdDAO {

    public void insertOrderId(Context mContext, int orderId, int userId){
        SQLiteDatabase db = DatabaseHelper.getsInstance(mContext).getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.ORDER_ID, orderId);
        cv.put(DatabaseHelper.USER_ID, userId);
        db.insertWithOnConflict(DatabaseHelper.TABLE_NAME_ORDER, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public ArrayList<String> getOrderId(Context mContext, String userId){
        ArrayList<String> data = new ArrayList<>();
        SQLiteDatabase db = DatabaseHelper.getsInstance(mContext).getReadableDatabase();
        String selection = DatabaseHelper.USER_ID + "=?";
        String[] selectionArgs = {userId};
        Cursor cursor = db.query(DatabaseHelper.TABLE_NAME_ORDER, null, selection, selectionArgs
                , null, null, null);

        while (cursor.moveToNext()){
            int orderId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.ORDER_ID));
            data.add(String.valueOf(orderId));
        }

        return data;
    }
}

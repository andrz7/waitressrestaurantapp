package thesis.traininlink.utilities;

import java.net.URL;

/**
 * Created by Andrea's on 9/21/2016.
 */

public class Constant {
    public static final String BASE_URL = "http://139.162.45.37";
    public static final String PHOTO_PATH = "/android_training/public/assets/item/";
    public static final String PROFILE_PHOTO_PATH = "/android_training/public/assets/photo/";
    public static final String REGISTER_PATH = "user/user_register";
    public static final String LOGIN_PATH = "user/user_login";
    public static final String FAILED_STATUS = "failed";
    public static final String KEU_ORDER_ID = "order_id";
    public static final int TOOLBAR_ELEVATION = 20;
    public static final int SPAN_COUNT = 2;
    public static final int DUMMY_USER_ID = 24;
    public static final String SOSMED_ACCOUNT_ZERO = "Do not have Social Media Account";

    public static final String INTENT_EMAIL_KEY_EXTRA = "email";
    public static final String INTENT_PASSWORD_KEY_EXTRA = "password";
}

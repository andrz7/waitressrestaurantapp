package thesis.traininlink.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Andrea's on 10/1/2016.
 */

public class SharedPreferenceHelper {

    static final String TOTAL_PRICE_ORDERED_ITEM_KEY = "total_price";
    static final String IS_FROM_REGISTER_KEY = "is_registered";
    static final String ORDER_ID_KEY = "order_id";

    static SharedPreferences getSharedPreferences(Context mContext){
        return PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public static void saveCurrentUserId(Context mContext, int orderId){
        SharedPreferences.Editor editor = getSharedPreferences(mContext).edit();
        editor.putInt(ORDER_ID_KEY, orderId);
        editor.commit();
    }

    public static int getCurrentOrderId(Context mContext){
        return getSharedPreferences(mContext).getInt(ORDER_ID_KEY, 0);
    }

    public static void deleteCurrentOrderId(Context mContext){
        SharedPreferences.Editor editor = getSharedPreferences(mContext).edit();
        editor.putInt(ORDER_ID_KEY, 0);
        editor.commit();
    }

    public static void saveTotalOrderedMenu(Context mContext, long total){
        SharedPreferences.Editor editor = getSharedPreferences(mContext).edit();
        editor.putLong(TOTAL_PRICE_ORDERED_ITEM_KEY, total);
        editor.commit();
    }

    public static long getTotalOrderedMenu(Context mContext){
        return getSharedPreferences(mContext).getLong(TOTAL_PRICE_ORDERED_ITEM_KEY, 0);
    }

    public static void setIsRegistered(Context mContext, boolean isRegisered){
        SharedPreferences.Editor editor = getSharedPreferences(mContext).edit();
        editor.putBoolean(IS_FROM_REGISTER_KEY, isRegisered);
        editor.commit();
    }

    public static boolean getRegistered(Context mContext){
        return getSharedPreferences(mContext).getBoolean(IS_FROM_REGISTER_KEY, false);
    }
}

package thesis.traininlink.services;

import android.telecom.Call;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import thesis.traininlink.form.AddItemForm;
import thesis.traininlink.form.MenuListForm;
import thesis.traininlink.form.RegisterForm;
import thesis.traininlink.form.SearchForm;
import thesis.traininlink.form.LoginForm;
import thesis.traininlink.form.UserIdentifier;
import thesis.traininlink.form.ViewOrderForm;
import thesis.traininlink.model.AddItemResponse;
import thesis.traininlink.model.CityNameResponse;
import thesis.traininlink.model.LoginResponse;
import thesis.traininlink.model.MenuListResponse;
import thesis.traininlink.model.OrderResponse;
import thesis.traininlink.model.RegisterResponse;
import thesis.traininlink.model.ViewOrderResponse;

import static android.R.attr.password;

/**
 * Created by Andrea's on 9/25/2016.
 */

public interface ClientServices {

    @POST("/android_training/public/loginMember")
    void loginMember(@Body LoginForm loginForm, Callback<LoginResponse> response);

    @POST("/android_training/public/getCity")
    void getCityId(@Body SearchForm search, Callback<CityNameResponse> response);

    @POST("/android_training/public/regisMember")
    void getRegisterMember(
            @Body RegisterForm form,
            Callback<RegisterResponse> response);

    @POST("/android_training/public/createOrder")
    void createOrder(@Body UserIdentifier identifier, Callback<OrderResponse> callback);

    @POST("/android_training/public/menuList")
    void getMenuList(@Body MenuListForm orderId, Callback<MenuListResponse> callback);

    @POST("/android_training/public/addItem")
    void addItem(@Body AddItemForm item, Callback<AddItemResponse> callback);

    @POST("/android_training/public/viewOrder")
    void viewOrder(@Body ViewOrderForm form, Callback<ViewOrderResponse> callback);
}

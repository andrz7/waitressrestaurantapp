package thesis.traininlink.services;

import android.content.Context;

import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import thesis.traininlink.utilities.Constant;

/**
 * Created by Andrea's on 9/25/2016.
 */

public class Client {
    private static ClientServices mClient;
    private static GsonBuilder gsonBuilder;

    private  Client() {
    }

    public static ClientServices getServices(Context context){
        if (mClient == null){
            RestAdapter.Builder builder = new RestAdapter.Builder();
            builder.setEndpoint(Constant.BASE_URL);
            builder.setLogLevel(RestAdapter.LogLevel.FULL);
            builder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("Content-Type", "application/json");
                }
            }).setClient(new OkClient(getOkHttpClient()));

            if(gsonBuilder != null)
                builder.setConverter(new GsonConverter(gsonBuilder.create()));

            RestAdapter adapter = builder.build();
            mClient = adapter.create(ClientServices.class);
        }

        return mClient;
    }

    public static OkHttpClient getOkHttpClient(){
        OkHttpClient client = getUnsafeHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);
        return client;
    }

    public static OkHttpClient getUnsafeHttpClient(){
        try {
            final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            } };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setSslSocketFactory(sslSocketFactory);
            okHttpClient.setHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return Constant.BASE_URL.contains(hostname);
                }
            });

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

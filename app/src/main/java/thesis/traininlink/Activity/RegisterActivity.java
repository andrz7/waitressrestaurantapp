package thesis.traininlink.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import thesis.traininlink.R;
import thesis.traininlink.utilities.Util;
import thesis.traininlink.controller.RegisterController;
import thesis.traininlink.model.Cities;
import thesis.traininlink.model.CityNameResponse;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{
    private int RESULT_LOAD_IMG = 1;

    private Toolbar toolbar;

    private EditText fullNameEdit, emailEdit, phoneNumberEdit, addressEdit, passwordEdit;
    private AutoCompleteTextView cityName;
    private ImageView profilePicture;
    private RadioButton radioButton;
    private RadioGroup genderGroup;
    private Button registerButton;

    private RegisterController mController;
    private ArrayAdapter adapter;

    private List<String> cityNames;
    private List<Cities> cities;

    private int chosenCityId;

    private String chosenCityName;
    private String imageBase64String;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
        mController = new RegisterController(this);
        profilePicture.setOnClickListener(this);
        registerButton.setOnClickListener(this);
        initToolbar();
        cityName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            private Timer timer;
            @Override
            public void afterTextChanged(final Editable editable) {
                if (timer != null){
                    timer.cancel();
                }
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        mController.getcityId(editable.toString());
                    }
                }, 1000);
            }
        });
        cityName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                chosenCityName = cityNames.get(i);
                chosenCityId = cities.get(i).getId();
            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.profile_image:
                mController.getImageFromGallery(RESULT_LOAD_IMG);
                break;
            case R.id.register_button:
                register();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            final Uri imageUri = data.getData();
            final InputStream imageStream = getContentResolver().openInputStream(imageUri);
            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            imageBase64String = Util.convertToBase64(selectedImage);
            profilePicture.setImageBitmap(selectedImage);
        }catch (Exception e){
            Log.v("error", e.getMessage());
        }
    }

    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.activity_register);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.toolbar.setElevation(20);
        }
    }

    public void showList(CityNameResponse response){
        cities = new ArrayList<>();
        cityNames = new ArrayList<>();
        cities = response.getCityData();
        for (Cities city : response.getCityData()){
            cityNames.add(city.getName());
        }
        adapter = new ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line, cityNames);
        cityName.setAdapter(adapter);
        cityName.showDropDown();
    }

    private void initView(){
        cityName = (AutoCompleteTextView) findViewById(R.id.city_id_et);
        fullNameEdit = (EditText) findViewById(R.id.full_name_et);
        emailEdit = (EditText) findViewById(R.id.mail_et_regis);
        phoneNumberEdit = (EditText) findViewById(R.id.phone_number_et);
        addressEdit = (EditText) findViewById(R.id.address_et);
        passwordEdit = (EditText) findViewById(R.id.password_et_regis);
        profilePicture = (ImageView) findViewById(R.id.profile_image);
        genderGroup = (RadioGroup) findViewById(R.id.gender_group);
        registerButton = (Button) findViewById(R.id.register_button);

    }

    private void register(){
        if (cityName.getText().toString().isEmpty()){
            Toast.makeText(this, "Please Choose City", Toast.LENGTH_SHORT).show();
        }else if (fullNameEdit.getText().toString().isEmpty()){
            Toast.makeText(this, "Please Fill Your name", Toast.LENGTH_SHORT).show();
        } else if (emailEdit.getText().toString().isEmpty()){
            Toast.makeText(this, "Please Fill Your Mail", Toast.LENGTH_SHORT).show();
        } else if (!mController.isValidEmailAddress(emailEdit.getText().toString())){
            Toast.makeText(this, "Your Email Address is Not Valid", Toast.LENGTH_SHORT).show();
        }else if (phoneNumberEdit.getText().toString().isEmpty()){
            Toast.makeText(this, "Please Fill Your Phone Number", Toast.LENGTH_SHORT).show();
        } else if (!mController.isValidPhoneNumber(phoneNumberEdit.getText().toString())){
            Toast.makeText(this, "Phone Number format must be 08xxxxxxxxx", Toast.LENGTH_SHORT).show();
        }else if (addressEdit.getText().toString().isEmpty()){
            Toast.makeText(this, "Please Fill Your address", Toast.LENGTH_SHORT).show();
        } else if (passwordEdit.getText().toString().isEmpty()){
            Toast.makeText(this, "Please Fill Your password", Toast.LENGTH_SHORT).show();
        } else if (passwordEdit.getText().toString().length() < 5){
            Toast.makeText(this, "Password Must Have At Least five Character", Toast.LENGTH_SHORT).show();
        } else if (genderGroup.getCheckedRadioButtonId() < 0){
            Toast.makeText(this, "Please Choose your gender", Toast.LENGTH_SHORT).show();
        }else {
            String fullName = fullNameEdit.getText().toString();
            String email = emailEdit.getText().toString();
            String phoneNumber = phoneNumberEdit.getText().toString();
            String address = addressEdit.getText().toString();
            String password = passwordEdit.getText().toString();
            String encriptedPass = Util.convertToMD5(password);
            int selectedGenderId = genderGroup.getCheckedRadioButtonId();
            radioButton = (RadioButton) findViewById(selectedGenderId);
            String selectedGender = radioButton.getText().toString();
            mController.registerAccount(
                    chosenCityId,
                    fullName,
                    selectedGender,
                    email,
                    phoneNumber,
                    address,
                    imageBase64String,
                    encriptedPass,
                    "",
                    "",
                    0,
                    0
            );
        }
    }

    public void intentLogin(){
        mController.intentLogin(emailEdit.getText().toString(), passwordEdit.getText().toString());
    }
}

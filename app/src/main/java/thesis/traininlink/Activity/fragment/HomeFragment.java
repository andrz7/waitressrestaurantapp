package thesis.traininlink.Activity.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import thesis.traininlink.Activity.HomeActivity;
import thesis.traininlink.Activity.ViewsOrderActivity;
import thesis.traininlink.R;
import thesis.traininlink.adapter.CustomClickListener;
import thesis.traininlink.adapter.HomeOrderListAdapter;
import thesis.traininlink.controller.HomeController;
import thesis.traininlink.database.UserDao;
import thesis.traininlink.model.User;
import thesis.traininlink.utilities.Constant;

/**
 * Created by Andrea's on 9/30/2016.
 */

public class HomeFragment extends Fragment implements CustomClickListener{
    private int SPAN_COUNT = 3;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.home_activity_order_list)
    RecyclerView orderRv;

    private GridLayoutManager layoutManager;
    private HomeOrderListAdapter adapter;
    private HomeController mController;
    private HomeActivity mActivity;
    private User user;

    private ArrayList<String> data;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (HomeActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity.setToolbarTitle(getString(R.string.home_activity), 20);
        mController = new HomeController(mActivity);
        user = mController.getUserData();
        data = mController.getOrderId(mActivity, String.valueOf(user.getId()));
//        data = mController.getOrderId(mActivity, String.valueOf(Constant.DUMMY_USER_ID));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = new HomeOrderListAdapter(mActivity, data, this);
        orderRv.setAdapter(adapter);
    }

    private void setRecyclerView() {
        layoutManager = new GridLayoutManager(
                mActivity,
                SPAN_COUNT,
                LinearLayoutManager.VERTICAL,
                false
        );
        orderRv.setLayoutManager(layoutManager);
    }

    @OnClick(R.id.fab)
    public void onFabClicked(){
        mController.createOrder(String.valueOf(user.getId()), user.getFullName());
    }

    @Override
    public void setOnItemClickListener(View view, int position) {
        Intent i = new Intent(mActivity, ViewsOrderActivity.class);
        i.putExtra(Constant.KEU_ORDER_ID, data.get(position));
        startActivity(i);
    }
}

package thesis.traininlink.Activity.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import thesis.traininlink.Activity.HomeActivity;
import thesis.traininlink.R;
import thesis.traininlink.controller.ProfileController;
import thesis.traininlink.model.User;
import thesis.traininlink.utilities.Constant;

import static java.lang.System.load;

/**
 * Created by Andrea's on 10/3/2016.
 */

public class ProfileFragment extends Fragment {

    @BindView(R.id.profile_picture)
    ImageView profilePicture;

    @BindView(R.id.address_tv)
    TextView addressTv;

    @BindView(R.id.full_name_tv)
    TextView fullNameTv;

    @BindView(R.id.gender_tv)
    TextView genderTv;

    @BindView(R.id.facebook_id_tv)
    TextView facebookIdTv;

    @BindView(R.id.twitter_id_tv)
    TextView twitterIdTv;

    @BindView(R.id.email_tv)
    TextView emailTv;

    private ProfileController mController;
    private HomeActivity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (HomeActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mController = new ProfileController(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment_layout, container, false);
        ButterKnife.bind(this, view);
        mActivity.setToolbarTitle(getString(R.string.profile_activity), 20);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mController.getUserData();
    }

    public void showProfile(User user){
        Glide.with(this)
                .load(Constant.BASE_URL + Constant.PROFILE_PHOTO_PATH + user.getAvatar())
                .error(R.drawable.ic_account_circle_black_24dp)
                .into(profilePicture);
        fullNameTv.setText(user.getFullName());
        genderTv.setText(user.getGender());
        addressTv.setText(user.getAddress());
        facebookIdTv.setText(user.getFacebookId() == null ? Constant.SOSMED_ACCOUNT_ZERO : user.getFacebookId());
        twitterIdTv.setText(user.getTwitterId() == null ? Constant.SOSMED_ACCOUNT_ZERO : user.getTwitterId());
        emailTv.setText(user.getEmail());
    }
}

package thesis.traininlink.Activity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import thesis.traininlink.Activity.TestingActivity;
import thesis.traininlink.R;
import thesis.traininlink.adapter.CustomClickListener;
import thesis.traininlink.adapter.FrgmentListener;
import thesis.traininlink.adapter.TransferAdapter;

/**
 * Created by Andrea's on 9/21/2016.
 */

public class RecyclerFragment extends Fragment {

    private LinearLayoutManager layoutManager;
    private TransferAdapter adapter;

    private RecyclerView dummyRv;

    private TestingActivity activity;
    private List<String> data;

    public RecyclerFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.recycler_fragment, container, false);
        data = new ArrayList<>();
        activity = (TestingActivity) getActivity();
        dummyRv = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        layoutManager = new LinearLayoutManager(getActivity());

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dummyRv.setLayoutManager(layoutManager);
        dummyRv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(activity, "hh", Toast.LENGTH_SHORT).show();
            }
        });
        adapter = new TransferAdapter(activity, data, new CustomClickListener() {
            @Override
            public void setOnItemClickListener(View view, int position) {
                Toast.makeText(activity.getApplicationContext(), "Dummy", Toast.LENGTH_SHORT).show();
            }
        }, new FrgmentListener() {
            @Override
            public void setOnClick(int value) {
                Log.v("as", "dummy");
                activity.changeFragment(value);
            }
        });
        dummyRv.setAdapter(adapter);
    }
}

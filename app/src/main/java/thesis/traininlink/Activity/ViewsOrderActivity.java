package thesis.traininlink.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import thesis.traininlink.R;
import thesis.traininlink.adapter.CustomClickListener;
import thesis.traininlink.adapter.MenuListAdapter;
import thesis.traininlink.adapter.ViewOrderAdapter;
import thesis.traininlink.controller.MenuListController;
import thesis.traininlink.controller.ViewOrderController;
import thesis.traininlink.model.Menu;
import thesis.traininlink.model.MenuListResponse;
import thesis.traininlink.model.OrderedMenus;
import thesis.traininlink.model.ViewOrderResponse;
import thesis.traininlink.utilities.Constant;

import static android.R.attr.data;

public class ViewsOrderActivity extends AppCompatActivity implements CustomClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.view_order_rv)
    RecyclerView viewOrderRv;

    private LinearLayoutManager layoutManager;
    private ViewOrderController mController;
    private ViewOrderAdapter adapter;

    private ArrayList<OrderedMenus> data;

    private String chosenOrderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_views_order);
        ButterKnife.bind(this);
        initObj();
        getBundle();
        setToolbar();
        setRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mController.viewOrder(chosenOrderId);
    }

    private void setToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            toolbar.setElevation(Constant.TOOLBAR_ELEVATION);
            getSupportActionBar().setTitle(R.string.view_order);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initObj() {
        mController = new ViewOrderController(this);
        data = new ArrayList<>();
    }

    private void getBundle(){
       Intent i = getIntent();
        chosenOrderId = i.getStringExtra(Constant.KEU_ORDER_ID);
    }

    private void setRecyclerView() {
        layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
        );
        adapter = new ViewOrderAdapter(this, data);

        viewOrderRv.setLayoutManager(layoutManager);
        viewOrderRv.setAdapter(adapter);
    }

    public void viewOrderDetail(ViewOrderResponse response){
        data = response.getData();
        adapter.setData(data);
    }

    @Override
    public void setOnItemClickListener(View view, int position) {

    }
}

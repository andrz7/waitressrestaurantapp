package thesis.traininlink.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import thesis.traininlink.R;
import thesis.traininlink.database.UserDao;
import thesis.traininlink.model.User;

public class DataList extends AppCompatActivity {

    UserDao dao;
    private TextView fullNameTv, genderTv, emailTv, cityTV, phoneTv, addressTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_list);
        dao = new UserDao();
        User data = dao.getData(this);
        Log.v("data", data.getFullName());
        fullNameTv = (TextView) findViewById(R.id.data_user_name_tv);
        genderTv = (TextView) findViewById(R.id.data_gender_tv);
        emailTv = (TextView) findViewById(R.id.data_email_tv);
        cityTV = (TextView) findViewById(R.id.data_city_tv);
        phoneTv = (TextView) findViewById(R.id.data_phone_tv);
        addressTv = (TextView) findViewById(R.id.data_city_tv);

        fullNameTv.setText(data.getFullName());
        genderTv.setText(data.getGender());
        emailTv.setText(data.getEmail());
        cityTV.setText(data.getCity());
        phoneTv.setText(data.getPhone());
        addressTv.setText(data.getAddress());
    }


}

package thesis.traininlink.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import thesis.traininlink.Activity.fragment.DummyFragment;
import thesis.traininlink.R;
import thesis.traininlink.Activity.fragment.RecyclerFragment;

public class TestingActivity extends AppCompatActivity {

    private int tempValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new RecyclerFragment()).commit();
    }

    public void changeFragment(int value){
        tempValue = value;
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new DummyFragment()).commit();
    }

    public int getTempValue() {
        return tempValue;
    }
}

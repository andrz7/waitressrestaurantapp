package thesis.traininlink.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import thesis.traininlink.Activity.fragment.HomeFragment;
import thesis.traininlink.Activity.fragment.ProfileFragment;
import thesis.traininlink.R;
import thesis.traininlink.database.UserDao;
import thesis.traininlink.model.User;
import thesis.traininlink.utilities.Constant;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.navigation)
    NavigationView navigationView;

    @BindView(R.id.drawer)
    DrawerLayout drawerLayout;

    private ImageView profileImage;
    private TextView userNameTv;

    private ActionBarDrawerToggle drawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_view_home, new HomeFragment());
        fragmentTransaction.commit();
        initToolbar();
        setUpDrawerContent();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.home);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.toolbar.setElevation(20);
        }
    }

    private ActionBarDrawerToggle setUpDrawerToggle(){
        return new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.open_navigation,
                R.string.close_navigation
        );
    }

    private void setUpDrawerContent(){
        User user = getUserData();

        View headerLayout = navigationView.inflateHeaderView(R.layout.navigation_header_layout);
        profileImage = (ImageView) headerLayout.findViewById(R.id.nav_profile_photo);
        userNameTv = (TextView) headerLayout.findViewById(R.id.nav_full_name_tv);

        Glide.with(this).load(Constant.BASE_URL + Constant.PROFILE_PHOTO_PATH + user.getAvatar()).into(profileImage);
        userNameTv.setText(user.getFullName());

        drawerToggle = setUpDrawerToggle();
        drawerLayout.addDrawerListener(drawerToggle);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectDrawerItem(item);
                return true;
            }
        });
    }

    private void selectDrawerItem(MenuItem item) {
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (item.getItemId()){
            case R.id.home:
                fragmentClass = HomeFragment.class;
                break;
            case R.id.profile:
                fragmentClass = ProfileFragment.class;
                break;
            case R.id.logout:
                UserDao dao = new UserDao();
                dao.deleteAllData(this);
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }
        if (item.getItemId() != R.id.logout) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_view_home, fragment).commit();
                item.setChecked(true);
                setTitle(item.getTitle());
                drawerLayout.closeDrawers();
            } else {
                Toast.makeText(HomeActivity.this, "On Progress...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setToolbarTitle(String title, int elevation){
        if (getSupportActionBar() != null){
            toolbar.setElevation(elevation);
            getSupportActionBar().setTitle(title);
        }
    }

    private User getUserData(){
        UserDao dao = new UserDao();
        return dao.getData(this);
    }
}

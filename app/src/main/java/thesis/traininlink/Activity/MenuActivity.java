package thesis.traininlink.Activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.akiniyalocts.pagingrecycler.PagingDelegate;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import thesis.traininlink.R;
import thesis.traininlink.adapter.CustomClickListener;
import thesis.traininlink.adapter.MenuConfirmationAdapter;
import thesis.traininlink.adapter.MenuListAdapter;
import thesis.traininlink.controller.MenuListController;
import thesis.traininlink.model.Menu;
import thesis.traininlink.model.MenuCart;
import thesis.traininlink.model.MenuListResponse;
import thesis.traininlink.utilities.Constant;

public class MenuActivity extends AppCompatActivity implements CustomClickListener, PagingDelegate.OnPageListener{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.menu_rv)
    RecyclerView menuRv;

    private GridLayoutManager layoutManager;
    private MenuListController mController;
    private MenuListResponse response;
    private MenuListAdapter adapter;
    private List<MenuCart> tempData;
    private ArrayList<Menu> data;

    private int DEFAULT_MENU_ID_PAGE = 0;
    private int DEFAULT_SHOWN_PAGE = 4;
    private int lastOrderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        setToolbar();
        initObj();
        setRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        lastOrderId = mController.getLastOrderId();
        mController.getMenuList(DEFAULT_MENU_ID_PAGE);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.confirm:
                showConfirmationDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setOnItemClickListener(View view, final int position) {
        new MaterialDialog.Builder(this)
                .title(R.string.input_amount_dialog_title)
                .content(R.string.input_amount_dialog_message)
                .positiveText("Add")
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .input(R.string.imput_amount_dialog_hint, R.string.input_amount_prefail_hint, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        if (input.toString().isEmpty()){
                            Toast.makeText(MenuActivity.this, "Please Type The Amount of Item"
                                    , Toast.LENGTH_SHORT).show();
                        }else{
                            int menuId = data.get(position).getFoodId();
                            int amount = Integer.parseInt(input.toString());
                            mController.addItem(lastOrderId, menuId, amount);
                            mController.saveToCart(
                                    menuId,
                                    amount,
                                    data.get(position).getFoodName(),
                                    data.get(position).getPrice(), tempData);
                            mController.calculateOrderTotalPrice(tempData);
                        }
                    }
                }).show();
    }

    private void showConfirmationDialog(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        MenuConfirmationAdapter adapter = new MenuConfirmationAdapter(this, tempData);
        new MaterialDialog.Builder(this)
                .title(R.string.dialog_confirmation_title)
                .content(R.string.dialog_confirmation_message)
                .positiveText("Sure")
                .adapter(adapter, layoutManager)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mController.backToHomeActivity(lastOrderId);
                    }
                }).show();
    }


    private void setToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            toolbar.setElevation(Constant.TOOLBAR_ELEVATION);
            getSupportActionBar().setTitle(R.string.menu_activity_title);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initObj() {
        mController = new MenuListController(this);
        tempData = new ArrayList<>();
        data = new ArrayList<>();
        adapter = new MenuListAdapter(this, data, this);
    }

    private void setRecyclerView() {
        layoutManager = new GridLayoutManager(
                this,
                Constant.SPAN_COUNT,
                LinearLayoutManager.VERTICAL,
                false
        );

        menuRv.setLayoutManager(layoutManager);
        PagingDelegate pagingDelegate = new PagingDelegate.Builder(adapter)
                .attachTo(menuRv)
                .listenWith(this)
                .build();
        menuRv.setAdapter(adapter);
    }

    public void showMenuList(MenuListResponse response){
        this.response = response;
        if (data.size() <= 0)
            data = response.getMenus();
        else
            data.addAll(response.getMenus());

        adapter.setData(data);
    }

    @Override
    public void onPage(int i) {
         if (response.isNextPage()){
            mController.getMenuList(response.getMenus().get(DEFAULT_SHOWN_PAGE).getFoodId());
        }
    }

    @Override
    public void onDonePaging() {

    }
}

package thesis.traininlink.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import thesis.traininlink.R;
import thesis.traininlink.database.UserDao;
import thesis.traininlink.model.User;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UserDao dao = new UserDao();
        User user = dao.getData(this);
        if (user != null){
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }else{
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }
}

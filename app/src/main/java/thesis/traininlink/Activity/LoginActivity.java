package thesis.traininlink.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import thesis.traininlink.R;
import thesis.traininlink.controller.LoginController;
import thesis.traininlink.utilities.Constant;
import thesis.traininlink.utilities.SharedPreferenceHelper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;

    private EditText emailEdit;
    private EditText passEdit;

    private TextView signUpText;

    private Button loginButton;

    private LoginController mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initToolbar();
        initView();
        mController = new LoginController(this);
        loginButton.setOnClickListener(this);
        signUpText.setOnClickListener(this);
        if (SharedPreferenceHelper.getRegistered(this)){
            Bundle b = getIntent().getExtras();
            SharedPreferenceHelper.setIsRegistered(this, false);
            mController.login(b.getString(Constant.INTENT_EMAIL_KEY_EXTRA, ""), b.getString(Constant.INTENT_PASSWORD_KEY_EXTRA, ""));
        }
    }

    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.activity_login);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.toolbar.setElevation(20);
        }
    }

    private void initView() {
        emailEdit = (EditText) findViewById(R.id.email_et);
        passEdit = (EditText) findViewById(R.id.password_et);
        loginButton = (Button) findViewById(R.id.login_button);
        signUpText = (TextView) findViewById(R.id.sign_up_tv);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_button:
                login();
                break;
            case R.id.sign_up_tv:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }

    private void login(){
        if (emailEdit.getText().toString().isEmpty()){
            Toast.makeText(this, "Fill Your Email address", Toast.LENGTH_SHORT).show();
        } else if (!mController.isValidEmailAddress(emailEdit.getText().toString())){
            Toast.makeText(this, "Email Address Is Not Valid", Toast.LENGTH_SHORT).show();
        }else if (passEdit.getText().toString().isEmpty()){
            Toast.makeText(this, "Fill your Password", Toast.LENGTH_SHORT).show();
        }else{
            mController.login(emailEdit.getText().toString(), passEdit.getText().toString());
        }
    }
}

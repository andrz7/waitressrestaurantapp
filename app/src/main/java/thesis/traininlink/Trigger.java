package thesis.traininlink;

/**
 * Created by Andrea's on 9/26/2016.
 */

public interface Trigger {
    void finish();
}

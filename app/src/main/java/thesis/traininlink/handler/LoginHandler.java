package thesis.traininlink.handler;

import android.content.Intent;
import android.os.Message;
import android.widget.Toast;

import thesis.traininlink.Activity.DataList;
import thesis.traininlink.Activity.HomeActivity;
import thesis.traininlink.Activity.LoginActivity;
import thesis.traininlink.database.UserDao;
import thesis.traininlink.model.LoginResponse;
import thesis.traininlink.model.User;

/**
 * Created by Andrea's on 9/25/2016.
 */

public class LoginHandler extends BaseHandler<LoginActivity> {
    public final static int WHAT_FAILED_LOGIN = 1;
    public final static int WHAT_INVALID_INPUT = 2;
    public final static int WHAT_LOGIN = 3;

    private LoginActivity mActivity;

    public LoginHandler(LoginActivity mParentComponent) {
        super(mParentComponent);
        mActivity = mParentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case WHAT_FAILED_LOGIN:
                showFailedLoginToast((LoginResponse) msg.obj);
                break;
            case WHAT_INVALID_INPUT:
                showInvalidInputToast();
                break;
            case WHAT_LOGIN:
                login((LoginResponse) msg.obj);
                break;
        }
        super.handleMessage(msg);
    }

    private void showInvalidInputToast() {
        Toast.makeText(mActivity, "Login Failed Please Check Your Input Again", Toast.LENGTH_SHORT).show();
    }

    private void showFailedLoginToast(LoginResponse response) {
        Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void login(LoginResponse response){
        User user = response.getUser();
        UserDao dao = new UserDao();
        dao.addNewTransaction(mActivity, user.getId(), user.getFullName(), user.getGender(), user.getEmail(), user.getCity(), user.getPhone()
                , user.getAddress(), user.getAvatar(), user.getFacebookId(), user.getTwitterId(), String.valueOf(user.getLatitude())
                , String.valueOf(user.getLongitude()));
        mActivity.startActivity(new Intent(mActivity, HomeActivity.class));
        mActivity.finish();
    }
}

package thesis.traininlink.handler;

import android.content.Intent;
import android.os.Message;

import thesis.traininlink.Activity.HomeActivity;
import thesis.traininlink.Activity.MenuActivity;
import thesis.traininlink.model.OrderResponse;
import thesis.traininlink.utilities.SharedPreferenceHelper;

/**
 * Created by Andrea's on 9/30/2016.
 */

public class HomeHandler extends BaseHandler<HomeActivity> {
    public static final int WHAT_INTENT_MENU = 1;

    private HomeActivity mActivity;

    public HomeHandler(HomeActivity mParentComponent) {
        super(mParentComponent);
        mActivity = mParentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what){
            case WHAT_INTENT_MENU:
                intentMenuActivity((OrderResponse) msg.obj);
                break;
        }
    }

    private void intentMenuActivity(OrderResponse response){
        saveCurrentOrderId(response.getId());
        mActivity.startActivity(new Intent(mActivity, MenuActivity.class));
    }

    private void saveCurrentOrderId(int orderId){
        SharedPreferenceHelper.saveCurrentUserId(mActivity, orderId);
    }
}

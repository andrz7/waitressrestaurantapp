package thesis.traininlink.handler;

import android.os.Message;
import android.widget.Toast;

import thesis.traininlink.Activity.MenuActivity;
import thesis.traininlink.model.MenuListResponse;

/**
 * Created by Andrea's on 10/1/2016.
 */

public class MenuHandler extends BaseHandler<MenuActivity> {

    public static final int WHAT_SHOW_MENU_LIST = 1;
    public static final int WHAT_SUCCESS_ADD_ITEM = 2;

    private MenuActivity mActivity;

    public MenuHandler(MenuActivity mParentComponent) {
        super(mParentComponent);
        mActivity = mParentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what){
            case WHAT_SHOW_MENU_LIST:
                showMenuList((MenuListResponse) msg.obj);
                break;
            case WHAT_SUCCESS_ADD_ITEM:
                Toast.makeText(mActivity, "Your Item has been added", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void showMenuList(MenuListResponse response){
        mActivity.showMenuList(response);
    }
}

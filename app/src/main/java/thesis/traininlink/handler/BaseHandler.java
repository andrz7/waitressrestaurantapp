package thesis.traininlink.handler;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Message;
import android.widget.Toast;

import thesis.traininlink.Application;

/**
 * Created by Andrea's on 9/25/2016.
 */

public class BaseHandler<T> extends android.os.Handler {
    public final static int WHAT_CONNECTION_TIMEOUT = -11;
    public final static int WHAT_NETWORK_ERROR = -2;

    private Application mApplication;
    private T mParentComponent;
    private Context mContext;


    @SuppressLint("NewApi")
    public BaseHandler(T mParentComponent) {
        this.mParentComponent = mParentComponent;
        if (mParentComponent instanceof Activity){
            mApplication = (Application) ((Activity) mParentComponent).getApplication();
        }else if (mParentComponent instanceof android.support.v4.app.Fragment){
            mApplication = (Application) ((android.support.v4.app.Fragment) mParentComponent).getActivity().getApplication();

        }else if (mParentComponent instanceof Fragment){
            mApplication = (Application) ((Fragment) mParentComponent).getActivity().getApplication();
        }else{
            throw new IllegalArgumentException("Parent component must be instance of activity, fragment, or support fragment");
        }
        mContext = mApplication.getApplicationContext();
    }

    @SuppressLint("NewApi")
    private Activity getParentActivity() {
        if(mParentComponent instanceof Activity) {
            return (Activity) mParentComponent;
        } else if(mParentComponent instanceof android.support.v4.app.Fragment) {
            return ((android.support.v4.app.Fragment) mParentComponent).getActivity();
        } else if(mParentComponent instanceof android.app.Fragment) {
            return ((android.app.Fragment) mParentComponent).getActivity();
        } else {
            return null;
        }
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case WHAT_NETWORK_ERROR:
                toastConnectionError();
                break;
            case WHAT_CONNECTION_TIMEOUT:
                toastConnectionTimeOut();
                break;
        }
        super.handleMessage(msg);
    }
    
    private void toastConnectionError(){
        Toast.makeText(mApplication, "Please check your connection...", Toast.LENGTH_SHORT).show();
    }

    private void toastConnectionTimeOut(){
        Toast.makeText(mApplication, "Connection Time Out", Toast.LENGTH_SHORT).show();
    }
}

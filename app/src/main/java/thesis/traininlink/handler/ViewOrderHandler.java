package thesis.traininlink.handler;

import android.os.Message;

import thesis.traininlink.Activity.ViewsOrderActivity;
import thesis.traininlink.model.ViewOrderResponse;

/**
 * Created by Andrea's on 10/3/2016.
 */

public class ViewOrderHandler extends BaseHandler<ViewsOrderActivity> {
    public static final int WHAT_VIEW_ORDER = 1;

    private ViewsOrderActivity mActivity;


    public ViewOrderHandler(ViewsOrderActivity mParentComponent) {
        super(mParentComponent);
        mActivity = mParentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what){
            case WHAT_VIEW_ORDER:
                showOrderDetail((ViewOrderResponse) msg.obj);
                break;
        }
    }

    private void showOrderDetail(ViewOrderResponse response){
        mActivity.viewOrderDetail(response);
    }
}

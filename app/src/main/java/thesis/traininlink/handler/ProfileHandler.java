package thesis.traininlink.handler;

import android.os.Message;

import thesis.traininlink.Activity.fragment.ProfileFragment;
import thesis.traininlink.model.User;

/**
 * Created by Andrea's on 10/4/2016.
 */

public class ProfileHandler extends BaseHandler<ProfileFragment> {
    public static final int WHAT_SHOW_PROFILE = 1;

    private ProfileFragment mFragment;

    public ProfileHandler(ProfileFragment mParentComponent) {
        super(mParentComponent);
        mFragment = mParentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what){
            case WHAT_SHOW_PROFILE:
                showProfile((User) msg.obj);
                break;
        }
    }

    private void showProfile(User user) {
        mFragment.showProfile(user);
    }
}

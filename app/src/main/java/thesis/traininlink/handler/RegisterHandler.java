package thesis.traininlink.handler;

import android.content.Intent;
import android.os.Message;
import android.widget.Toast;

import thesis.traininlink.Activity.LoginActivity;
import thesis.traininlink.Activity.RegisterActivity;
import thesis.traininlink.model.CityNameResponse;
import thesis.traininlink.model.RegisterResponse;
import thesis.traininlink.utilities.SharedPreferenceHelper;

/**
 * Created by Andrea's on 9/25/2016.
 */

public class RegisterHandler extends BaseHandler<RegisterActivity> {
    public static final int SHOW_PREDICTION_LIST = 1;
    public static final int WHAT_REGISTER_ACCOUNT = 2;
    public static final int WHAT_REGISTER_FAILED = 3;

    private RegisterActivity mActivity;

    public RegisterHandler(RegisterActivity mParentComponent) {
        super(mParentComponent);
        this.mActivity = mParentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what){
            case SHOW_PREDICTION_LIST:
                showPredictionList((CityNameResponse) msg.obj);
                break;
            case WHAT_REGISTER_ACCOUNT:
                showRegisterSuccess();
                break;
            case WHAT_REGISTER_FAILED:
                showRegisterFailedToast((RegisterResponse) msg.obj);
                break;
        }
    }

    private void showPredictionList(CityNameResponse response) {
        mActivity.showList(response);
    }

    private void showRegisterSuccess(){
        SharedPreferenceHelper.setIsRegistered(mActivity, true);
        Toast.makeText(mActivity, "Register Success", Toast.LENGTH_SHORT).show();
        mActivity.intentLogin();
    }

    private void showRegisterFailedToast(RegisterResponse response){
        Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_SHORT).show();
    }
}

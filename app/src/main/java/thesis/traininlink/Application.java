package thesis.traininlink;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;



/**
 * Created by Andrea's on 9/25/2016.
 */

public class Application extends android.app.Application {
    private Class<? extends Activity> mActivityClass;
    private Context context;
    private Handler mhandler;

    @Override
    public void onCreate() {
        super.onCreate();
        mhandler = new Handler();
        context = getApplicationContext();
        enableStheto();
    }

    public void enableStheto(){
       /* Stetho.InitializerBuilder initializerBuilder =
                Stetho.newInitializerBuilder(this);
        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this)
        );
        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(this)
        );
        Stetho.Initializer initializer = initializerBuilder.build();
        Stetho.initialize(initializer);*/
    }
}

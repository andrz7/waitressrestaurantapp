package thesis.traininlink.controller;

import android.app.Activity;
import android.content.Context;
import android.os.Message;

import com.bumptech.glide.Glide;

import thesis.traininlink.Activity.HomeActivity;
import thesis.traininlink.Activity.fragment.ProfileFragment;
import thesis.traininlink.database.UserDao;
import thesis.traininlink.handler.ProfileHandler;
import thesis.traininlink.model.User;

/**
 * Created by Andrea's on 10/4/2016.
 */

public class ProfileController {
    private ProfileFragment mFragment;
    private ProfileHandler mHandler;
    private Context mContext;

    public ProfileController(ProfileFragment mFragment) {
        this.mFragment = mFragment;
        mContext = mFragment.getActivity().getApplicationContext();
        mHandler = new ProfileHandler(mFragment);
    }

    public void getUserData(){
        UserDao dao = new UserDao();
        User user = dao.getData(mContext);
        sendMessageShowProfile(user);
    }

    private void sendMessageShowProfile(User user){
        if (user != null){
            Message msg = new Message();
            msg.what = mHandler.WHAT_SHOW_PROFILE;
            msg.obj = user;
            mHandler.sendMessage(msg);
        }
    }

}

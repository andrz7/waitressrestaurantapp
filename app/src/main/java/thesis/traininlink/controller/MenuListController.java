package thesis.traininlink.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import thesis.traininlink.Activity.HomeActivity;
import thesis.traininlink.Activity.MenuActivity;
import thesis.traininlink.database.OrderIdDAO;
import thesis.traininlink.database.UserDao;
import thesis.traininlink.form.AddItemForm;
import thesis.traininlink.form.MenuListForm;
import thesis.traininlink.handler.BaseHandler;
import thesis.traininlink.handler.MenuHandler;
import thesis.traininlink.model.AddItemResponse;
import thesis.traininlink.model.Menu;
import thesis.traininlink.model.MenuCart;
import thesis.traininlink.model.MenuListResponse;
import thesis.traininlink.model.User;
import thesis.traininlink.services.Client;
import thesis.traininlink.services.ClientServices;
import thesis.traininlink.utilities.Constant;
import thesis.traininlink.utilities.SharedPreferenceHelper;

import static android.R.attr.data;
import static android.R.id.input;

/**
 * Created by Andrea's on 10/1/2016.
 */

public class MenuListController {

    private static final String TAG = "ERROR";
    private MenuActivity mActivity;
    private MenuHandler mHandler;
    private Context mContext;

    public MenuListController(MenuActivity activity) {
        mActivity = activity;
        mContext = mActivity.getApplicationContext();
        mHandler = new MenuHandler(mActivity);
    }

    public int getLastOrderId(){
        return SharedPreferenceHelper.getCurrentOrderId(mContext);
    }

    public void getMenuList(int orderId){
        MenuListForm form = new MenuListForm(String.valueOf(orderId));
        ClientServices services = Client.getServices(mContext);
        services.getMenuList(form, new Callback<MenuListResponse>() {
            @Override
            public void success(MenuListResponse menuListResponse, Response response) {
                sendMenuListMessage(menuListResponse);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void sendMenuListMessage(MenuListResponse response){
        if (response != null){
            Message msg = new Message();
            msg.what = mHandler.WHAT_SHOW_MENU_LIST;
            msg.obj = response;
            mHandler.sendMessage(msg);
        }
    }

    public void addItem(int orderId, int menuId, int amount){
        AddItemForm item = new AddItemForm(orderId, menuId, amount);
        ClientServices services = Client.getServices(mContext);
        services.addItem(item, new Callback<AddItemResponse>() {
            @Override
            public void success(AddItemResponse addItemResponse, Response response) {
                mHandler.sendEmptyMessage(mHandler.WHAT_SUCCESS_ADD_ITEM);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.e(TAG, "failure : " + error.getResponse().getBody());
                    Log.e(TAG, "failure : " + error.getBody());
                    Log.e(TAG, "failure body : " + new Gson().toJson(error.getBody()));
                    Log.e(TAG, "failure url: " + error.getUrl());
                    Log.e(TAG, "catch : " + error.getMessage());
                } catch (Exception e) {
                    Log.e(TAG, "catch exception: " + e.getMessage());
                }

                Message message = new Message();

                if (error.isNetworkError()) {
                    if (error.getCause() instanceof SocketTimeoutException) {
                        message.what = BaseHandler.WHAT_CONNECTION_TIMEOUT;
                    } else {
                        message.what = BaseHandler.WHAT_NETWORK_ERROR;
                    }
                }
                mHandler.sendMessage(message);
            }
        });
    }

    public void backToHomeActivity(int orderId){
        int userId = getUserId();
        savetoInternalDB(orderId, userId);
        Intent intent = new Intent(mActivity, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mActivity.startActivity(intent);
    }

    private int getUserId(){
        UserDao dao = new UserDao();
        User user = dao.getData(mContext);
        return user.getId();
    }

    private void savetoInternalDB(int orderId, int userId){
        OrderIdDAO dao = new OrderIdDAO();
        dao.insertOrderId(mActivity, orderId, userId);
    }

    public void saveToCart(int menuId, int amount, String foodName, long foodPrice,  List<MenuCart> data){
        int itemPosition = isItemExist(menuId, data);
        long totalItemPricePerFood = amount * foodPrice;

        if (itemPosition < 0){
            data.add(new MenuCart(menuId, amount, foodName, totalItemPricePerFood));
        } else if (itemPosition >= 0){
            MenuCart cart = data.get(itemPosition);
            cart.setFoodName(foodName);
            cart.setFoodPrice(totalItemPricePerFood);
            cart.setAmount(amount);
            data.set(itemPosition, cart);
        }
    }

    private int isItemExist(int menuId, List<MenuCart> data){
        int position = 0;
        while (position < data.size()){
            if (data.get(position).getMenuId() == menuId)
                return position;
            position++;
        }
        return -1;
    }

    public void calculateOrderTotalPrice(List<MenuCart> data){
        long totalPrice = 0;
        for (MenuCart cart : data){
            totalPrice += cart.getFoodPrice();
        }
        SharedPreferenceHelper.saveTotalOrderedMenu(mContext, totalPrice);
    }
}

package thesis.traininlink.controller;

import android.content.Context;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;

import java.net.SocketTimeoutException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import thesis.traininlink.Activity.ViewsOrderActivity;
import thesis.traininlink.form.ViewOrderForm;
import thesis.traininlink.handler.BaseHandler;
import thesis.traininlink.handler.ViewOrderHandler;
import thesis.traininlink.model.ViewOrderResponse;
import thesis.traininlink.services.Client;
import thesis.traininlink.services.ClientServices;

/**
 * Created by Andrea's on 10/3/2016.
 */

public class ViewOrderController {
    private static final String TAG = "ERROR";
    private ViewsOrderActivity mActivity;
    private ViewOrderHandler mHandler;
    private Context mContext;

    public ViewOrderController(ViewsOrderActivity mActivity) {
        this.mActivity = mActivity;
        mContext = mActivity.getApplicationContext();
        mHandler = new ViewOrderHandler(mActivity);
    }

    public void viewOrder(String orderId){
        ViewOrderForm form = new ViewOrderForm(orderId);
        ClientServices services = Client.getServices(mContext);
        services.viewOrder(form, new Callback<ViewOrderResponse>() {
            @Override
            public void success(ViewOrderResponse viewOrderResponse, Response response) {
                sendViewOrderMessage(viewOrderResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.e(TAG, "failure : " + error.getResponse().getBody());
                    Log.e(TAG, "failure : " + error.getBody());
                    Log.e(TAG, "failure body : " + new Gson().toJson(error.getBody()));
                    Log.e(TAG, "failure url: " + error.getUrl());
                    Log.e(TAG, "catch : " + error.getMessage());
                } catch (Exception e) {
                    Log.e(TAG, "catch exception: " + e.getMessage());
                }

                Message message = new Message();

                if (error.isNetworkError()) {
                    if (error.getCause() instanceof SocketTimeoutException) {
                        message.what = BaseHandler.WHAT_CONNECTION_TIMEOUT;
                    } else {
                        message.what = BaseHandler.WHAT_NETWORK_ERROR;
                    }
                }
            }
        });

    }

    private void sendViewOrderMessage(ViewOrderResponse response) {
        if (response != null){
            Message msg = new Message();
            msg.what = ViewOrderHandler.WHAT_VIEW_ORDER;
            msg.obj = response;
            mHandler.sendMessage(msg);
        }
    }
}

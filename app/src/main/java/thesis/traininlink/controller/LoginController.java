package thesis.traininlink.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;

import java.net.SocketTimeoutException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import thesis.traininlink.Activity.LoginActivity;
import thesis.traininlink.R;
import thesis.traininlink.handler.BaseHandler;
import thesis.traininlink.utilities.Util;
import thesis.traininlink.form.LoginForm;
import thesis.traininlink.handler.LoginHandler;
import thesis.traininlink.model.LoginResponse;
import thesis.traininlink.services.Client;
import thesis.traininlink.services.ClientServices;

/**
 * Created by Andrea's on 9/25/2016.
 */

public class LoginController {
    private static final String TAG = "ERROR";
    private ProgressDialog progress;
    private LoginActivity mActivity;
    private LoginHandler mHandler;
    private Context mContext;

    public LoginController(LoginActivity mActivity) {
        this.mActivity = mActivity;
        mContext = mActivity.getApplicationContext();
        mHandler = new LoginHandler(mActivity);
    }

    public void login(String email, String password){
        LoginForm form = new LoginForm(email, Util.convertToMD5(password));
            progress = ProgressDialog.show(mActivity, mActivity.getResources().getString(R.string.loading),
                    mActivity.getResources().getString(R.string.loading_mess), true);
            ClientServices clientServices = Client.getServices(mContext);
            clientServices.loginMember(form, new Callback<LoginResponse>() {
                @Override
                public void success(LoginResponse loginResponse, Response response) {
                    if (loginResponse != null){
                        if (loginResponse.getStatus().equals("failed")){
                            sendLoginFailedMessage(loginResponse);
                        } else {
                            sendLoginMessage(loginResponse);
                        }
                        progress.dismiss();
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    progress.dismiss();
                    try {
                        Log.e(TAG, "failure : " + error.getResponse().getBody());
                        Log.e(TAG, "failure : " + error.getBody());
                        Log.e(TAG, "failure body : " + new Gson().toJson(error.getBody()));
                        Log.e(TAG, "failure url: " + error.getUrl());
                        Log.e(TAG, "catch : " + error.getMessage());
                    } catch (Exception e) {
                        Log.e(TAG, "catch exception: " + e.getMessage());
                    }

                    Message message = new Message();
                    if (error != null) {
                        if (error.isNetworkError()) {
                            if (error.getCause() instanceof SocketTimeoutException) {
                                message.what = BaseHandler.WHAT_CONNECTION_TIMEOUT;
                            } else {
                                message.what = BaseHandler.WHAT_NETWORK_ERROR;
                            }
                        }
                    }
                    mHandler.sendMessage(message);
                }
            });
    }

    private void sendLoginFailedMessage(LoginResponse response){
        Message msg = new Message();
        msg.what = LoginHandler.WHAT_FAILED_LOGIN;
        msg.obj = response;
        mHandler.sendMessage(msg);
    }

    private void sendLoginMessage(LoginResponse response){
        if (response != null){
            Message message = new Message();
            message.what = LoginHandler.WHAT_LOGIN;
            message.obj = response;
            mHandler.sendMessage(message);
        }
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}

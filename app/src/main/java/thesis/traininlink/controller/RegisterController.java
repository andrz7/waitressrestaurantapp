package thesis.traininlink.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.SocketTimeoutException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import thesis.traininlink.Activity.LoginActivity;
import thesis.traininlink.Activity.RegisterActivity;
import thesis.traininlink.R;
import thesis.traininlink.form.RegisterForm;
import thesis.traininlink.form.SearchForm;
import thesis.traininlink.handler.BaseHandler;
import thesis.traininlink.handler.RegisterHandler;
import thesis.traininlink.model.CityNameResponse;
import thesis.traininlink.model.RegisterResponse;
import thesis.traininlink.services.Client;
import thesis.traininlink.services.ClientServices;
import thesis.traininlink.utilities.Constant;

/**
 * Created by Andrea's on 9/25/2016.
 */

public class RegisterController {
    private static final String TAG = "ERROR";
    private ProgressDialog progress;
    private RegisterActivity mActivity;
    private RegisterHandler mHandler;
    private Context mContext;

    public RegisterController(RegisterActivity mActivity) {
        this.mActivity = mActivity;
        mHandler = new RegisterHandler(mActivity);
        mContext = mActivity.getApplicationContext();
    }

    public void getcityId(String cityName){

        SearchForm form = new SearchForm(cityName);
        ClientServices services = Client.getServices(mContext);
        services.getCityId(form, new Callback<CityNameResponse>() {
            @Override
            public void success(CityNameResponse cityNameResponse, Response response) {
                sendShowPredictionMessage(cityNameResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("error", error.getMessage());
            }
        });
    }

    public void registerAccount(int cityId, String fullName, String gender, String email
            , String phone, String address, String photo, String password, String facebookId,
                                String twitterId, double latitude, double longitude){
        progress = ProgressDialog.show(mActivity, mActivity.getResources().getString(R.string.loading),
                mActivity.getResources().getString(R.string.loading_mess), true);

        RegisterForm form = new RegisterForm(String.valueOf(cityId), fullName, gender, email, phone
                , address, photo, password, facebookId, twitterId, String.valueOf(latitude), String.valueOf(longitude));

            ClientServices clientServices = Client.getServices(mContext);
            clientServices.getRegisterMember(form, new Callback<RegisterResponse>() {
                @Override
                public void success(RegisterResponse registerResponse, Response response) {
                    if (registerResponse.getStatus().equals("failed"))
                        sendFailedRegisterMessage(registerResponse);
                    else
                        sendRegisterMessage(registerResponse);
                    progress.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    progress.dismiss();

                    try {
                        Log.e(TAG, "failure : " + error.getResponse().getBody());
                        Log.e(TAG, "failure : " + error.getBody());
                        Log.e(TAG, "failure body : " + new Gson().toJson(error.getBody()));
                        Log.e(TAG, "failure url: " + error.getUrl());
                        Log.e(TAG, "catch : " + error.getMessage());
                    } catch (Exception e) {
                        Log.e(TAG, "catch exception: " + e.getMessage());
                    }

                    Message message = new Message();

                    if (error.isNetworkError()) {
                        if (error.getCause() instanceof SocketTimeoutException) {
                            message.what = BaseHandler.WHAT_CONNECTION_TIMEOUT;
                        } else {
                            message.what = BaseHandler.WHAT_NETWORK_ERROR;
                        }
                    }
                    mHandler.sendMessage(message);
                }
            });
    }

    private String convertGsonToJson(RegisterForm form){
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String placeJSON = gson.toJson(form);
        return placeJSON;
    }

    private void sendShowPredictionMessage(CityNameResponse cityNameResponse){
        if (cityNameResponse != null){
            Message msg = new Message();
            msg.what = RegisterHandler.SHOW_PREDICTION_LIST;
            msg.obj = cityNameResponse;
            mHandler.sendMessage(msg);
        }
    }

    public void getImageFromGallery(int requestCode){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        mActivity.startActivityForResult(photoPickerIntent, requestCode);
    }

    public void sendRegisterMessage(RegisterResponse response){
        if (response != null){
            Message msg = new Message();
            msg.obj = response;
            msg.what = RegisterHandler.WHAT_REGISTER_ACCOUNT;
            mHandler.sendMessage(msg);
        }
    }

    private void sendFailedRegisterMessage(RegisterResponse response){
        if (response != null){
            Message msg = new Message();
            msg.obj = response;
            msg.what = mHandler.WHAT_REGISTER_FAILED;
            mHandler.sendMessage(msg);
        }
    }

    public void intentLogin(String email, String password){
        Intent intent = new Intent(mActivity, LoginActivity.class);
        intent.putExtra(Constant.INTENT_EMAIL_KEY_EXTRA, email);
        intent.putExtra(Constant.INTENT_PASSWORD_KEY_EXTRA, password);
        mActivity.startActivity(intent);
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public boolean isValidPhoneNumber(String phoneNumber){
        if (phoneNumber.charAt(0) == '0' && phoneNumber.charAt(1) == '8')
            return true;
        return false;
    }
}

package thesis.traininlink.controller;

import android.content.Context;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import thesis.traininlink.Activity.HomeActivity;
import thesis.traininlink.Application;
import thesis.traininlink.database.OrderIdDAO;
import thesis.traininlink.database.UserDao;
import thesis.traininlink.form.UserIdentifier;
import thesis.traininlink.handler.BaseHandler;
import thesis.traininlink.handler.HomeHandler;
import thesis.traininlink.model.OrderResponse;
import thesis.traininlink.model.User;
import thesis.traininlink.services.Client;
import thesis.traininlink.services.ClientServices;

import static android.R.id.message;

/**
 * Created by Andrea's on 9/30/2016.
 */

public class HomeController {
    private static final String TAG = "ERROR";
    private HomeActivity mActivity;
    private HomeHandler mHandler;
    private Context mContext;

    public HomeController(HomeActivity mActivity) {
        this.mActivity = mActivity;
        mContext = mActivity.getApplicationContext();
        mHandler = new HomeHandler(mActivity);
    }

    public void createOrder(String userId, String userName){
        UserIdentifier identifier = new UserIdentifier(userId, userName);
        ClientServices clientServices = Client.getServices(mContext);
        clientServices.createOrder(identifier, new Callback<OrderResponse>() {
            @Override
            public void success(OrderResponse orderResponse, Response response) {
                sendMessageIntentToMenuActivity(orderResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.e(TAG, "failure : " + error.getResponse().getBody());
                    Log.e(TAG, "failure : " + error.getBody());
                    Log.e(TAG, "failure body : " + new Gson().toJson(error.getBody()));
                    Log.e(TAG, "failure url: " + error.getUrl());
                    Log.e(TAG, "catch : " + error.getMessage());
                } catch (Exception e) {
                    Log.e(TAG, "catch exception: " + e.getMessage());
                }

                Message message = new Message();

                if (error.isNetworkError()) {
                    if (error.getCause() instanceof SocketTimeoutException) {
                        message.what = BaseHandler.WHAT_CONNECTION_TIMEOUT;
                    } else {
                        message.what = BaseHandler.WHAT_NETWORK_ERROR;
                    }
                }
                mHandler.sendMessage(message);
            }
        });
    }

    public User getUserData(){
        UserDao dao = new UserDao();
        User user = dao.getData(mContext);
        return user;
    }

    public ArrayList<String> getOrderId(Context mContext, String userId){
        OrderIdDAO dao = new OrderIdDAO();
        return dao.getOrderId(mContext, userId);
    }

    private void sendMessageIntentToMenuActivity(OrderResponse orderResponse){
        if (orderResponse != null){
            Message msg = new Message();
            msg.what = HomeHandler.WHAT_INTENT_MENU;
            msg.obj = orderResponse;
            mHandler.sendMessage(msg);
        }
    }
}

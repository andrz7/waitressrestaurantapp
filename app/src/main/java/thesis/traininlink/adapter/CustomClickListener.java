package thesis.traininlink.adapter;

import android.view.View;

/**
 * Created by Andrea's on 9/21/2016.
 */

public interface CustomClickListener {
    void setOnItemClickListener(View view, int position);
}

package thesis.traininlink.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import thesis.traininlink.R;

/**
 * Created by Andrea's on 9/19/2016.
 */
public class TransferAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private int HEADER_VIEW = 1;
    private int NORMAL_VIEW = 2;
    private int FOOTER_VIEW = 3;

    private int counter = 0;
    private boolean isShown = false;

    private FrgmentListener frgmentListener;
    private CustomClickListener listener;
    private List<String> data;
    private Activity activity;

    public TransferAdapter(Activity activity, List<String> data, CustomClickListener listener, FrgmentListener fragmentListener) {
        this.data = data;
        this.listener = listener;
        this.activity = activity;
        this.frgmentListener = fragmentListener;
    }

    public void setData(List<String> data){
        this.data = data;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == NORMAL_VIEW){
            View view = LayoutInflater.from(activity).inflate(R.layout.list_item, parent, false);
            final NormalHolder normalHolder = new NormalHolder(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.setOnItemClickListener(view, normalHolder.getPosition());
                }
            });
            return normalHolder;
        } else if (viewType == FOOTER_VIEW) {
            View view = LayoutInflater.from(activity).inflate(R.layout.footer, parent, false);
            return new FooterHolder(view);
        }else{
            View view = LayoutInflater.from(activity).inflate(R.layout.header, parent, false);
            return new HeaderHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof HeaderHolder){
            HeaderHolder headerHolder = (HeaderHolder) holder;
            headerHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (data.size() >=  5)
                        isShown = true;
                    data.add("dummy");
                    notifyDataSetChanged();
                }
            });
        } else if (holder instanceof NormalHolder) {
            NormalHolder normalHolder = (NormalHolder) holder;
            normalHolder.dummyTv.setText(data.get(position - 1));
        } else if (holder instanceof  FooterHolder) {
            FooterHolder footerHolder = (FooterHolder) holder;
            if (isShown){
                footerHolder.footerButton.setVisibility(View.VISIBLE);
                footerHolder.footerButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        frgmentListener.setOnClick(data.size());
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)){
            return HEADER_VIEW;
        } else if (isPositionFooter(position)){
            return FOOTER_VIEW;
        }

        return NORMAL_VIEW;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == data.size() + 1;
    }

    class HeaderHolder extends RecyclerView.ViewHolder{
        private Button button;
        public HeaderHolder(View itemView) {
            super(itemView);
            button = (Button) itemView.findViewById(R.id.click);
        }
    }

    class NormalHolder extends RecyclerView.ViewHolder{
        private TextView dummyTv;
        public NormalHolder(View itemView) {
            super(itemView);
            dummyTv = (TextView) itemView.findViewById(R.id.text);
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder{
        private Button footerButton;
        public FooterHolder(View itemView) {
            super(itemView);
            footerButton = (Button) itemView.findViewById(R.id.footer_button);
        }
    }
}

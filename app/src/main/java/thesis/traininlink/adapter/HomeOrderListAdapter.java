package thesis.traininlink.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import thesis.traininlink.R;

/**
 * Created by Andrea's on 10/2/2016.
 */

public class HomeOrderListAdapter extends RecyclerView.Adapter<HomeOrderListAdapter.Holder>{
    private CustomClickListener listener;
    private ArrayList<String> data;
    private Context mContext;

    public HomeOrderListAdapter(Context mContext, ArrayList<String> data
            , CustomClickListener listener) {
        this.mContext = mContext;
        this.data = data;
        this.listener = listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).
                inflate(R.layout.home_order_list_card_item, parent, false);
        final Holder holder = new Holder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.setOnItemClickListener(view, holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.orderIdTv.setText(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        private TextView orderIdTv;

        public Holder(View itemView) {
            super(itemView);
            orderIdTv = (TextView) itemView.findViewById(R.id.home_order_id_tv);
        }
    }
}

package thesis.traininlink.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import thesis.traininlink.R;
import thesis.traininlink.model.Menu;
import thesis.traininlink.model.MenuCart;
import thesis.traininlink.utilities.SharedPreferenceHelper;

/**
 * Created by Andrea's on 10/5/2016.
 */

public class MenuConfirmationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int HEADER_VIEW = 1;
    private int NORMAL_VIEW = 2;
    private int FOOTER_VIEW = 3;

    private List<MenuCart> chosenMenu;
    private Context mContext;

    public MenuConfirmationAdapter(Context mContext, List<MenuCart> chosenMenu) {
        this.mContext = mContext;
        this.chosenMenu = chosenMenu;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.confirmation_header_layout, parent, false);
            return new HeaderHolder(view);
        } else if (viewType == NORMAL_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.confirmation_body_recycler_item, parent, false);
            return new NormalHolder(view);
        } else if (viewType == FOOTER_VIEW) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.confirmation_footer_layout, parent, false);
            return new FooterHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderHolder){

        } else if (holder instanceof NormalHolder) {
            NormalHolder normalHolder = (NormalHolder) holder;
            normalHolder.numberTv.setText(String.valueOf(position));
            normalHolder.foodNameTv.setText(chosenMenu.get(position - 1).getFoodName());
            normalHolder.qtyTv.setText(String.valueOf(chosenMenu.get(position - 1).getAmount()));
            normalHolder.priceTv.setText(String.format("%,d", chosenMenu.get(position - 1).getFoodPrice()));
        } else if (holder instanceof FooterHolder) {
            FooterHolder footerHolder = (FooterHolder) holder;
            footerHolder.totalTv.setText(String.format("IDR %,d", SharedPreferenceHelper.getTotalOrderedMenu(mContext)));
        }
    }

    @Override
    public int getItemCount() {
        return chosenMenu.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)){
            return HEADER_VIEW;
        } else if (isPositionFooter(position)){
            return FOOTER_VIEW;
        }

        return NORMAL_VIEW;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == chosenMenu.size() + 1;
    }

    class NormalHolder extends RecyclerView.ViewHolder{
        private TextView numberTv, foodNameTv, qtyTv, priceTv;

        public NormalHolder(View itemView) {
            super(itemView);
            numberTv = (TextView) itemView.findViewById(R.id.confirmation_no);
            foodNameTv = (TextView) itemView.findViewById(R.id.confirmation_food_name);
            qtyTv = (TextView) itemView.findViewById(R.id.confirmation_food_qty);
            priceTv = (TextView) itemView.findViewById(R.id.confirmation_food_price);
        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder{

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder{
        private TextView totalTv;

        public FooterHolder(View itemView) {
            super(itemView);
            totalTv = (TextView) itemView.findViewById(R.id.confirmation_total_tv);
        }
    }
}

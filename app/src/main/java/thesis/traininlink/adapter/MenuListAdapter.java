package thesis.traininlink.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.akiniyalocts.pagingrecycler.PagingAdapter;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import thesis.traininlink.R;
import thesis.traininlink.model.Menu;
import thesis.traininlink.utilities.Constant;

/**
 * Created by Andrea's on 10/1/2016.
 */

public class MenuListAdapter extends PagingAdapter{

    private ArrayList<Menu> data;
    private Context mContext;

    private CustomClickListener listener;

    public MenuListAdapter(Context mContext, ArrayList<Menu> data, CustomClickListener listener) {
        this.mContext = mContext;
        this.data = data;
        this.listener = listener;
    }

    public void setData(ArrayList<Menu> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.menu_list_card_item, parent, false);
        final Holder holder = new Holder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.setOnItemClickListener(view, holder.getPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Holder normalHolder = (Holder) holder;
        normalHolder.foodNameTv.setText(data.get(position).getFoodName());
        normalHolder.foodCategoryTv.setText(data.get(position).getFoodType());
        normalHolder.priceTv.setText(String.format("IDR %,d.00", data.get(position).getPrice()));
        normalHolder.loadImage(Constant.BASE_URL + Constant.PHOTO_PATH + data.get(position).getFoodImagePath());
    }

    @Override
    public int getPagingLayout() {
        return R.layout.toolbar;
    }

    @Override
    public int getPagingItemCount() {
        return data.size();
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    public class Holder extends RecyclerView.ViewHolder{
        private TextView foodNameTv, foodCategoryTv, priceTv;
        private ImageView foodImage;

        public Holder(View itemView) {
            super(itemView);
            foodCategoryTv = (TextView) itemView.findViewById(R.id.category_tv);
            foodNameTv = (TextView) itemView.findViewById(R.id.food_name_tv);
            foodImage = (ImageView) itemView.findViewById(R.id.food_image);
            priceTv = (TextView) itemView.findViewById(R.id.price_tv);
        }

        private void loadImage(String url){
            Glide.with(mContext).load(url).into(foodImage);
        }
    }


}

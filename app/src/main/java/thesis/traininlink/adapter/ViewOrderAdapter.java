package thesis.traininlink.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import thesis.traininlink.R;
import thesis.traininlink.model.OrderedMenus;
import thesis.traininlink.utilities.Constant;

/**
 * Created by Andrea's on 10/5/2016.
 */

public class ViewOrderAdapter extends RecyclerView.Adapter<ViewOrderAdapter.Holder>{

    private ArrayList<OrderedMenus> data;
    private Context mContext;

    public ViewOrderAdapter(Context mContext, ArrayList<OrderedMenus> data) {
        this.mContext = mContext;
        this.data = data;
    }

    public void setData(ArrayList<OrderedMenus> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_order_card_item, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Glide.with(mContext).load(Constant.BASE_URL + Constant.PHOTO_PATH + data.get(position).getFoodImagePath())
                .into(holder.foodImage);
        holder.priceTv.setText(String.format("IDR %,d.00", data.get(position).getPrice()));
        holder.amountTv.setText(String.valueOf(data.get(position).getQty()));
        holder.foodNameTv.setText(data.get(position).getFoodName());
        holder.typeTv.setText(data.get(position).getFoodType());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        private TextView foodNameTv, amountTv, priceTv, typeTv;
        private ImageView foodImage;

        public Holder(View itemView) {
            super(itemView);
            foodNameTv = (TextView) itemView.findViewById(R.id.view_order_food_name_tv);
            amountTv = (TextView) itemView.findViewById(R.id.view_order_amount_tv);
            priceTv = (TextView) itemView.findViewById(R.id.view_order_price_tv);
            typeTv = (TextView) itemView.findViewById(R.id.view_order_type_tv);
            foodImage = (ImageView) itemView.findViewById(R.id.view_order_iv);
        }
    }
}

package thesis.traininlink.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrea's on 10/5/2016.
 */

public class OrderedMenus {

    @SerializedName("id")
    private int foodId;

    @SerializedName("name")
    private String foodName;

    @SerializedName("type")
    private String foodType;

    @SerializedName("price")
    private long price;

    @SerializedName("image")
    private String foodImagePath;

    @SerializedName("amount")
    private int qty;

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getFoodImagePath() {
        return foodImagePath;
    }

    public void setFoodImagePath(String foodImagePath) {
        this.foodImagePath = foodImagePath;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

}

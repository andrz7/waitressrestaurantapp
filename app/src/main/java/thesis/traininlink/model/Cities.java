package thesis.traininlink.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrea's on 9/26/2016.
 */

public class Cities {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package thesis.traininlink.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrea's on 10/1/2016.
 */

public class MenuListResponse {

    @SerializedName("result")
    private String result;

    @SerializedName("nextPage")
    private boolean nextPage;

    @SerializedName("menus")
    private ArrayList<Menu> menus;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isNextPage() {
        return nextPage;
    }

    public void setNextPage(boolean nextPage) {
        this.nextPage = nextPage;
    }

    public ArrayList<Menu> getMenus() {
        return menus;
    }

    public void setMenus(ArrayList<Menu> menus) {
        this.menus = menus;
    }
}

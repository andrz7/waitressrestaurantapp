package thesis.traininlink.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Andrea's on 10/3/2016.
 */

public class ViewOrderResponse {

    @SerializedName("result")
    private String result;

    @SerializedName("name")
    private String name;

    @SerializedName("menus")
    private ArrayList<OrderedMenus> data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<OrderedMenus> getData() {
        return data;
    }

    public void setData(ArrayList<OrderedMenus> data) {
        this.data = data;
    }
}

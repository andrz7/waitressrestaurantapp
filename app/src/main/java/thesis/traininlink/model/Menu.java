package thesis.traininlink.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrea's on 10/1/2016.
 */

public class Menu {

    @SerializedName("id")
    private int foodId;

    @SerializedName("name")
    private String foodName;

    @SerializedName("type")
    private String foodType;

    @SerializedName("price")
    private long price;

    @SerializedName("image")
    private String foodImagePath;

    private int qty;

    private long totalPrices;

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getFoodImagePath() {
        return foodImagePath;
    }

    public void setFoodImagePath(String foodImagePath) {
        this.foodImagePath = foodImagePath;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public long getTotalPrices() {
        return totalPrices;
    }

    public void setTotalPrices(long totalPrices) {
        this.totalPrices = totalPrices;
    }
}

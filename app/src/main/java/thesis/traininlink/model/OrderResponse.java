package thesis.traininlink.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrea's on 9/30/2016.
 */

public class OrderResponse {
    @SerializedName("result")
    private String result;
    @SerializedName("id")
    private int id;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

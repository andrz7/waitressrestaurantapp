package thesis.traininlink.model;

/**
 * Created by Andrea's on 10/7/2016.
 */

public class MenuCart {
    private int menuId, amount;

    private String foodName;

    private long foodPrice;

    public MenuCart(int menuId, int amount, String foodName, long foodPrice) {
        this.menuId = menuId;
        this.amount = amount;
        this.foodName = foodName;
        this.foodPrice = foodPrice;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public long getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(long foodPrice) {
        this.foodPrice = foodPrice;
    }


}

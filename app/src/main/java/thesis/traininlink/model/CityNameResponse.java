package thesis.traininlink.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Andrea's on 9/26/2016.
 */

public class CityNameResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("cities")
    private ArrayList<Cities> cityData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Cities> getCityData() {
        return cityData;
    }

    public void setCityData(ArrayList<Cities> cityData) {
        this.cityData = cityData;
    }
}

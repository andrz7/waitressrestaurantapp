package thesis.traininlink.model;

import com.google.gson.annotations.SerializedName;

import static android.R.id.message;

/**
 * Created by Andrea's on 9/26/2016.
 */

public class RegisterResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("userid")
    private int userid;

    public int getUserid() {
        return status == "failed" ? userid : 0;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return status == "failed" ? message : "no message";
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

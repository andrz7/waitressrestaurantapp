package thesis.traininlink.form;

import android.text.TextUtils;

/**
 * Created by Andrea's on 9/25/2016.
 */

public class LoginForm {
    private String email;
    private String password;

    public LoginForm(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isValid(){
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){
            return true;
        }
        return false;
    }
}

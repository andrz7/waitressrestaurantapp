package thesis.traininlink.form;

/**
 * Created by Andrea's on 9/26/2016.
 */

public class SearchForm {
    private String search;

    public SearchForm(String search) {
        this.search = search;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}

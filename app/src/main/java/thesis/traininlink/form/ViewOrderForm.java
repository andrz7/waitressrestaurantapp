package thesis.traininlink.form;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrea's on 10/3/2016.
 */

public class ViewOrderForm {
    @SerializedName("orderid")
    private String orderId;

    public ViewOrderForm(String orderId) {
        this.orderId = orderId;
    }
}

package thesis.traininlink.form;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrea's on 10/2/2016.
 */

public class AddItemForm {

    @SerializedName("orderid")
    private int orderId;

    @SerializedName("menuid")
    private int menuId;

    @SerializedName("amount")
    private int amount;

    public AddItemForm(int orderId, int menuId, int amount) {
        this.orderId = orderId;
        this.menuId = menuId;
        this.amount = amount;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}

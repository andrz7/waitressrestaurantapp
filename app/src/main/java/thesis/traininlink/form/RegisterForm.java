package thesis.traininlink.form;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrea's on 9/26/2016.
 */

public class RegisterForm {

    @SerializedName("cityid")
    public String cityId;

    @SerializedName("fullname")
    public String fullName;

    @SerializedName("gender")
    public String gender;

    @SerializedName("email")
    public String email;

    @SerializedName("phone")
    public String  phone;

    @SerializedName("address")
    public String address;

    @SerializedName("photo")
    public String photo;

    @SerializedName("password")
    public String password;

    @SerializedName("facebookid")
    public String facebookId;

    @SerializedName("twitterid")
    public String twitterId;

    @SerializedName("latitude")
    public String latitude;

    @SerializedName("longitude")
    public String longitude;

    public RegisterForm(String cityId, String fullName, String gender, String email, String phone, String address, String photo,
                        String password, String facebookId, String twitterId, String latitude, String longitude) {
        this.cityId = cityId;
        this.fullName = fullName;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.photo = photo;
        this.password = password;
        this.facebookId = facebookId;
        this.twitterId = twitterId;
        this.latitude = latitude;
        this.longitude = longitude;

    }

    public boolean isValid(){
        if (!TextUtils.isEmpty(cityId) && !TextUtils.isEmpty(fullName) &&
                !TextUtils.isEmpty(gender) && !TextUtils.isEmpty(email) &&
                !TextUtils.isEmpty(phone) && !TextUtils.isEmpty(address) &&
                !TextUtils.isEmpty(password)){
            return true;
        }
        return false;
    }

    public String getAddress() {
        return address;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}

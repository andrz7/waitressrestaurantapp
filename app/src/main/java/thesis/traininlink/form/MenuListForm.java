package thesis.traininlink.form;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrea's on 10/2/2016.
 */

public class MenuListForm {

    @SerializedName("lastid")
    private String lastId;

    public MenuListForm(String lastId) {
        this.lastId = lastId;
    }
}
